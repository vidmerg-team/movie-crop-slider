module.exports = function(grunt) {

    grunt.initConfig({
        less: {
            main: {
                files: {
                    "css/movie.crop.css": "css/movie.crop.less"
                }
            },
            compress: {
                options: {
                    cleancss: true,
                    compress: true
                },
                files: {
                    "css/movie.crop.min.css": "css/movie.crop.css"
                }
            }
        },

        watch: {
            less: {
                files: [
                    'css/*.less'
                ],
                tasks: 'less'
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-watch');

    grunt.registerTask('default', ['less']);
};
