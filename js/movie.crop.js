﻿// Movie Crop.js
// version 1.0.0 RC
// =====================================================================================================================

var MovieCrop = function (wrap, config) {
    this.config = config;
    this.tm = null;
    this.range = null;
    this.data = null;

    this.$cache = {
        wrap: $(wrap),
        base: null,
        frames: null,
        cover: null,
        ribbon: null,
        input: null
    };

    this.cl = {
        // hooks
        base: ".js-mc",
        frames: ".js-mc-frames",
        cover: ".mc-frames-cover",
        input: ".js-mc-range",
        ribbon: ".js-mc-ribbon",

        // styles
        hidden: "mc-frames-ribbon_state_hidden",
        loading: "mc-frames-cover_state_loading",
        loaded: "mc-frames-cover_state_loaded"
    };

    this.range_config = {
        type: "double",
        min: 0,
        max: this.config.full_length,
        from: this.config.start_from,
        to: this.config.start_from + this.config.crop_length,
        step: 0.001,

        min_interval: this.config.crop_length,
        max_interval: this.config.crop_length,
        drag_interval: true,

        grid: true,
        grid_num: 5,

        hide_min_max: true,
        force_edges: true,

        onStart: this.change.bind(this),
        onFinish: this.change.bind(this),
        onUpdate: this.change.bind(this),

        prettify: function (num) {
            var minutes = Math.floor(num / 60).toString(),
                seconds = Math.floor(num - (minutes * 60)).toString(),
                mili = num.toString().split("."),
                miliseconds = mili.length > 1 ? mili[1] : "000",
                result;

            minutes = minutes.length < 2 ? "0" + minutes : minutes;
            seconds = seconds.length < 2 ? "0" + seconds : seconds;
            miliseconds = miliseconds.length < 2 ? miliseconds + "00" : miliseconds;
            miliseconds = miliseconds.length < 3 ? miliseconds + "0" : miliseconds;

            result = minutes + ":" + seconds + "." + miliseconds;

            return result;
        }
    };

    this.tpl =
        '<div class="mc js-mc">' +
        '<div class="mc-frames js-mc-frames">' +
        '<div class="mc-frames-cover mc-frames-cover_state_loading"></div>' +
        '<div class="mc-frames-range"><input type="text" value="" class="mc-frames-range__input js-mc-range" /></div>' +
        '</div>' +
        '</div>';

    this.tpl_frames_header = '<div class="mc-frames-ribbon mc-frames-ribbon_state_hidden js-mc-ribbon">';
    this.tpl_frames_item = function (frame) {
        return '<div class="mc-frames-ribbon__item" style="background-image: url(' + frame + ');"></div>'
    };
    this.tpl_frames_footer = '</div>';
};



MovieCrop.prototype = {
    init: function () {
        this.$cache.wrap.append(this.tpl);
        this.$cache.base = this.$cache.wrap.find(this.cl.base);
        this.$cache.frames = this.$cache.wrap.find(this.cl.frames);
        this.$cache.cover = this.$cache.wrap.find(this.cl.cover);
        this.$cache.input = this.$cache.wrap.find(this.cl.input);

        if (this.config.frames && this.config.frames.length) {
            this.createFrames();
        }
    },

    createFrames: function () {
        var frames = this.config.frames,
            num = frames.length,
            html = '',
            i;

        html += this.tpl_frames_header;
        for (i = 0; i < num; i++) {
            html += this.tpl_frames_item(frames[i]);
        }
        html += this.tpl_frames_footer;

        this.$cache.frames.prepend(html);
        this.$cache.ribbon = this.$cache.frames.find(this.cl.ribbon);

        this.tm = setTimeout(this.showFrames.bind(this), 500);
    },

    showFrames: function () {
        clearTimeout(this.tm);
        this.tm = null;

        this.$cache.ribbon.removeClass(this.cl.hidden);

        this.tm = setTimeout(this.removeLoader.bind(this), 500);
    },

    removeLoader: function () {
        clearTimeout(this.tm);
        this.tm = null;

        this.$cache.cover.removeClass(this.cl.loading);
        this.tm = setTimeout(this.addMask.bind(this), 100);
    },

    addMask: function () {
        clearTimeout(this.tm);
        this.tm = null;

        this.$cache.cover.addClass(this.cl.loaded);

        this.createRange();
    },

    createRange: function () {
        this.$cache.input.ionRangeSlider(this.range_config);
        this.range = this.$cache.input.data("ionRangeSlider");
    },

    change: function (data) {
        var c = this.config;

        if (data.to !== data.from + c.crop_length) {
            data.to = data.from + c.crop_length;
            data.to = +data.to.toFixed(3);
        }

        if (c.change && typeof c.change === "function") {
            if (c.scope) {
                c.change.call(c.scope, data);
            } else {
                c.change(data);
            }
        }
    }
};

/**
 * Create an instance
 */
exports.init = function() {
  var obj = Object.create(MovieCrop.prototype);
  MovieCrop.apply(obj, arguments);
  return obj;
};

module.exports = exports;
